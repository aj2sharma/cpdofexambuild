package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

public class MinMaxTest {

	@Test
    public void testGenerateResultWithAMoreThanB()  {
		
		int result = new MinMax().generateResult(2, 1);
		assertEquals(2, result);
	}
	
	@Test
    public void testGenerateResultWithALessThanB()  {
		
		int result = new MinMax().generateResult(2, 3);
		assertEquals(3, result);
	}
	
}
	
